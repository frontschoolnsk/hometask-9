const { CreateColor, useColor, Song } = require('./tasks');

describe('CreateColor', () => {
    test('должен возвращать объект', () => {
        expect(typeof new CreateColor('')).toBe('object')
    });
    test('в вернувшемся объекте должно быть поле с названием цвета', () => {
        const redColor = new CreateColor('красный');
        expect(redColor.colorName).toBe('красный')
    });
    test('метод use должен возвращать строку про использование', () => {
        const redColor = new CreateColor('красный');
        expect(redColor.use()).toBe('Используется красный цвет')
    });
    test('метод stopUse должен возвращать строку про окончание использования', () => {
        const yellowColor = new CreateColor('желтый');
        expect(yellowColor.stopUse()).toBe('Желтый цвет больше не используется')
    });
    test('метод use должен корректно работать при вызове в контексте другого объекта', () => {
        const yellowColor = new CreateColor('желтый');
        const blackColor = {
            colorName: 'черный',
            use: yellowColor.use,
        };
        expect(blackColor.use()).toBe('Используется черный цвет')
    });
    test('метод stopUse должен корректно работать при вызове в контексте другого объекта', () => {
        const yellowColor = new CreateColor('желтый');
        expect(yellowColor.stopUse.call({colorName: 'голубой'})).toBe('Голубой цвет больше не используется')
    });
    test('метод use должен приводить название к нижнему регистру', () => {
        const yellowColor = new CreateColor('жЕлТыЙ');
        expect(yellowColor.use()).toBe('Используется желтый цвет')
    });
    test('метод stopUse должен приводить название к нижнему регистру, а первую букву делать заглавной', () => {
        const greenColor = new CreateColor('зеЛеный');
        expect(greenColor.stopUse()).toBe('Зеленый цвет больше не используется')
    });
});

describe('useColor', () => {
    test('addMethod result', () => {
        expect(useColor.addMethod()).toBe('Используется серо-буро-малиновый в крапинку цвет')
    });
    test('useCall result', () => {
        expect(useColor.useCall()).toBe('Используется серо-буро-малиновый в крапинку цвет')
    });
    test('useBind result', () => {
        expect(useColor.useBind()).toBe('Используется серо-буро-малиновый в крапинку цвет')
    });
});

describe('Song', () => {
  test('поля title, album и author заполняются правильно', () => {
      const someSong = new Song('New Divide', 'Linkin Park', 'Transformers: Revenge of the Fallen – The Album');
      expect(someSong.title).toBe('New Divide');
      expect(someSong.author).toBe('Linkin Park');
      expect(someSong.album).toBe('Transformers: Revenge of the Fallen – The Album');
  });
  test('метод getFullName работает правильно', () => {
      const someSong = new Song('Kill the DJ', 'Green Day', '¡Uno!');
      expect(someSong.getFullName()).toBe('композиция «Kill the DJ», исполнитель Green Day, альбом «¡Uno!»');
  });
  test('метод setYear работает правильно', () => {
      const someSong = new Song('Kill the DJ', 'Green Day', '¡Uno!');
      someSong.setYear(2012);
      expect(someSong.year).toBe(2012);
  });
  test('метод setTitle работает правильно', () => {
      const someSong = new Song('Kill the DJ', 'Green Day', '¡Uno!');
      someSong.setTitle('Nuclear Family');
      expect(someSong.title).toBe('Nuclear Family');
  });
  test('метод getFullName работает правильно после изменения названия', () => {
      const someSong = new Song('Kill the DJ', 'Green Day', '¡Uno!');
      someSong.setTitle('Nuclear Family');
      expect(someSong.getFullName()).toBe('композиция «Nuclear Family», исполнитель Green Day, альбом «¡Uno!»');
  });
});

